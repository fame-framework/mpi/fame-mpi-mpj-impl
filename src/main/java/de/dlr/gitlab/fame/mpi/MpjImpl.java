package de.dlr.gitlab.fame.mpi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.logging.Logging;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;

/**Implements {@link MpiFacade} via MPJ Express parallelization
 * 
 * @author , Christoph Schimeczek, A. Achraf El Ghazi
 *
 */
public class MpjImpl implements MpiFacade {
	static final String OPEN_MPI_ERROR = "Error from OpenMPI: ";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MpjImpl.class);
	private int rank;
	private int size;
	
	@Override
	public MpiMode getMode() {
		return MpiMode.PARALLEL;
	}

	@Override
	public String[] initialise(String[] args) {
		try {
			String[] remainingArgs = MPI.Init(args);
			rank = MPI.COMM_WORLD.Rank();
			size = MPI.COMM_WORLD.Size();
			return remainingArgs;
		} catch (MPIException e) {
			throw Logging.logFatalException(LOGGER, OPEN_MPI_ERROR + e.getMessage());
		}
	}

	@Override
	public int getRank() {
		return rank;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public byte[] broadcastBytes(byte[] data, int source) {
		int dataLength = (rank == source) ? data.length : 0; 
		int length = broadcastInt(dataLength, source);
		if (rank != source) {
			data = new byte[length];
		}
		MPI.COMM_WORLD.Bcast(data, 0, length, MPI.BYTE, source);
		return data;
	}
	
	/** Copies an {@link Integer} from source to all processes
	 * 
	 * @return On all processes: the copied int */
	private int broadcastInt(int data, int source) {
		int[] buffer = new int[1];
		if (rank == source) {
			buffer[0] = data;
		}
		MPI.COMM_WORLD.Bcast(buffer, 0, 1, MPI.INT, source);
		return buffer[0];
	}

	@Override
	public void sendBytesTo(byte[] data, int target, int tag) {
		MPI.COMM_WORLD.Send(data, 0, data.length, MPI.BYTE, target, tag);
	}

	@Override
	public byte[] receiveBytesWithTag(int tag) {
		Status mpiStatus = MPI.COMM_WORLD.Probe(MPI.ANY_SOURCE, tag);
		int length = mpiStatus.Get_count(MPI.BYTE);
		int source = mpiStatus.source;
		byte[] bytes = new byte[length];
		MPI.COMM_WORLD.Recv(bytes, 0, length, MPI.BYTE, source, tag);
		return bytes;
	}

	@Override
	public MpjRequest iSendBytesTo(byte[] data, int target, int tag) {
		try {
			return new MpjRequest(MPI.COMM_WORLD.Isend(data, 0, data.length, MPI.BYTE, target, tag));
		} catch (MPIException e) {
			throw new RuntimeException(OPEN_MPI_ERROR + e.getMessage());
		}
	}
	
	@Override
	public void invokeFinalize() {
		try {
			MPI.Finalize();
		} catch (MPIException e) {
			throw Logging.logFatalException(LOGGER, OPEN_MPI_ERROR + e.getMessage());
		}
	}
	
}
