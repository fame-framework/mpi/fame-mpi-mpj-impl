/**
 * 
 */
package de.dlr.gitlab.fame.mpi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.logging.Logging;
import mpi.MPIException;
import mpi.Request;

/**
 * @author Christoph Schimeczek, A. Achraf El Ghazi
 *
 */
public class MpjRequest implements MpiRequestFacade {
	static final String WAIT_EXCEPTION = "Wait for request completion failed due to: ";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MpjRequest.class);
	
	private Request request;
	
	/** Creates an OpenMPI request-handler
	 * 
	 * @param request to handle */
	public MpjRequest(Request request) {
		this.request = request;
	}

	@Override
	public void waitForCompletion() {
		try {
			request.Wait();
		} catch (MPIException e) {
			Logging.logAndThrowFatal(LOGGER, WAIT_EXCEPTION + e.getMessage());
		}
	}

}
